import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { FirebaseModule } from './firebase.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        FirebaseModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})

export class AppModule { }
